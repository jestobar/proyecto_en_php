<?php

//inicializar los errores para ue se desplieguen
ini_set('display_errors',1);
ini_set('display_starup_error',1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

session_start();

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__. '/..');
$dotenv->load();



use Illuminate\Database\Capsule\Manager as Capsule;
use Aura\Router\RouterContainer;



//elocuent


$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => getenv('DB_DRIVE'),
    'host'      => getenv('DB_HOST'),
    'database'  => getenv('DB_NAME'),
    'username'  => getenv('DB_USER'),
    'password'  => getenv('DB_PASS'),
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();
$map->get('index', '/',[
    'controller'=> 'App\Controllers\IndexController',
    'action' => 'indexAction',
    
]);
$map->get('addJobs', '/jobs/add',
[
    'controller'=> 'App\Controllers\JobsController',
    'action' => 'getAddJobAction'
    ,'auth'=> true
]
);

$map->post('saveJobs', '/jobs/add',
[
    'controller'=> 'App\Controllers\JobsController',
    'action' => 'getAddJobAction'
]
);
$map->get('getuser', '/login/add',
[
    'controller'=> 'App\Controllers\loginController',
    'action' => 'getAddloginAction',
   
]
);

$map->post('saveuser', '/login/add',
[
    'controller'=> 'App\Controllers\loginController',
    'action' => 'getAddloginAction'
    
]
);
$map->get('addJLoginForm', '/login',
[
    'controller'=> 'App\Controllers\AuthController',
    'action' => 'getlogin'
]
);
$map->post('auth', '/auth',
[
    'controller'=> 'App\Controllers\AuthController',
    'action' => 'postLogin'
]
);
$map->get('admin', '/admin',
[
    'controller'=> 'App\Controllers\AdminController',
    'action' => 'getindex',
    'auth'=> true
]
);

$map->get('logaout', '/logout',
[
    'controller'=> 'App\Controllers\AuthController',
    'action' => 'getlogout'
]
);

$matcher = $routerContainer->getMatcher();

$route = $matcher->match($request);

if(!$route){
    echo 'No route';
}else {
   // require $route->handler;
   $handlerData = $route->handler;
   $controllerName = $handlerData['controller'];
   $actionName = $handlerData['action'];
   $needsAuth = $handlerData['auth'] ?? false;
   
   $sessionUserId = $_SESSION['userId'] ?? null;
    if ($needsAuth && !$sessionUserId ){
        echo 'Protected route';
        die;

    }

   $controller = new $controllerName ;
   $response =  $controller ->$actionName($request);
   foreach ($response->getHeaders() as $name =>$values )
   {
       foreach($values as $value){
           header(sprintf('%s: %s',$name,$value),false);
       }
   }
   http_response_code($response->getStatusCode());
   echo $response -> getBody();
}

// $route = $_GET['route'] ?? '/';
// if ($route == '/'){
//     require '../index.php';

// }elseif($route == 'addJob'){
//     require '../addJob.php';
// }

function printElement($job) {
    // if($job->visible == false) {
    //   return;
    // }
  
    echo '<li class="work-position">';
    echo '<h5>' . $job->title . '</h5>';
    echo '<p>' . $job->description . '</p>';
    echo '<p>' . $job->getDurationAsString() . '</p>';
    echo '<strong>Achievements:</strong>';
    echo '<ul>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '<li>Lorem ipsum dolor sit amet, 80% consectetuer adipiscing elit.</li>';
    echo '</ul>';
    echo '</li>';
  }