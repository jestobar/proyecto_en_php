<?php

require_once 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as Capsule;
use App\models\Project;



$capsule = new Capsule;

$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'cursophp',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();

// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$capsule->bootEloquent();


if (!empty($_POST)){
    $proyect = new Project();
    $proyect ->title = $_POST['title'];
    $proyect ->description = $_POST['description'];
    $proyect->save();
}







?>


<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Job</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B"
    crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">

</head>
<body>
    <form action="addProject.php" method="post">
        <div style="display: flex;align-items: center;flex-direction: column;">
            <h1>Add Proyect</h1>
       
            <label for="">Title:</label>
            <input  type="text" name="title" ><br>
        
        
            <label for="">Description:</label>
            <input type="text" name="description" ><br>
            <button type="submit" >Save</button>
        </div>
        
    </form>
    
</body>
</html>