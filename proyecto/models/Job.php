<?php 

namespace App\models;
use Illuminate\Database\Eloquent\Model;

class Job extends Model{

    protected $table = 'jobs';

    public function getdurationAsString(){
            
        $years = floor($this->months /12) ;
        $extraMonths = $this->months % 12 ;
        
        
        return "Job duration $years years $extraMonths months";
    
    } 


    
}