<?php

namespace App\models;
require_once 'Printable.php';

class baseElement {
    private $title;
    public $description;
    public $visible = true;
    public $months;

    public function setTitle($tit){
        if($tit == ''){

            $this->title = 'N/A';

        }else{
            $this->title = $tit;
        }
        
    }
  
    public function getTitle() {
        return $this->title;
    }

    public function getdurationAsString(){
    
        
        
        $years = floor($this->months /12) ;
        $extraMonths = $this->months % 12 ;
        
        
        return "$years years $extraMonths months";
    
    }
    public function __construct($title,$description){

        $this->setTitle($title);
        $this->description = $description;



    }

    public function getDescription(){
        return $this->description;
    }
    
}