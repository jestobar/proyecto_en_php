<?php
namespace App\Controllers;
use App\models\{login};
use Respect\Validation\Validator as v;

class loginController extends BaseController{
    public function getAddloginAction($request){

        $responseMessage = null ;
       
        if ($request->getMethod() == 'POST' ){
            $postData = $request->getParsedBody();
            $loginValidator = v::key('Correo', v::email()->notEmpty()->notOptional())
                  ->key('pass', v::stringType()->notEmpty()->notOptional()->length(6,22));
            
           // var_dump(password_hash($loginValidator,PASSWORD_DEFAULT)) ;
            
            try{

                $loginValidator->assert($postData);// true
                   $postData = $request->getParsedBody();    

                $log = new login();
                $log ->Correo = $postData['Correo'];
                $log ->pass = password_hash($postData['pass'],PASSWORD_DEFAULT) ;
                $log->save();
                $responseMessage = 'Saved';

            }catch(\Exception $e){

                $responseMessage = ('El correo y la contraseña estan de forma incorrecta '. $e);

            }
            
            
            
         
        }

        return $this->renderHTML('loginadd.twig',
    [
        'responseMessage'=> $responseMessage
    ]
    );
        //include '../views/addJob.php';

    }
}