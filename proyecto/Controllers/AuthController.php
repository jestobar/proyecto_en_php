<?php
namespace App\Controllers;
use App\models\{login};
use Respect\Validation\Validator as v;
use Zend\Diactoros\Response\RedirectResponse;

class AuthController extends BaseController{
    public function getlogin($request){
        return $this->renderHTML('login.twig'
        );
    }

    public function postlogin($request)
    {
      $responseMessage = null;  

      $postData = $request->getParsedBody();
      $user =  login::where('Correo',$postData['Correo'])->first();
      if($user){
        if(password_verify($postData['pass'],$user->pass))
        {
            $_SESSION['userId'] = $user->id;
            return new RedirectResponse('/admin');

        }else{
            $responseMessage = 'Bad credentials';
        }
      }  else{
            $responseMessage = 'Bad credentials';
      }


      return $this->renderHTML('login.twig',[
        'responseMessage' => $responseMessage
      ]);      
        

    }
    public function getlogout($request){
     unset($_SESSION['userId']);
      return new RedirectResponse('/login');

  }

}